#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>


class Elem {
public:
    Elem() { std::cout << "Elem()" << std::endl; }
    virtual ~Elem() { std::cout << "~Elem()" << std::endl;}
    Elem(Elem const& cp) {
        std::cout << "Elem(Elem)" << std::endl;
        *this = cp;
    }
    Elem& operator=(Elem const& cp) {
        std::cout << "Elem::operator=" << std::endl;
        if (this != &cp) {
        }
        return *this;
    }
    // remove the chance to move
    Elem(Elem&& mv) = delete;
    Elem& operator=(Elem&& mv) = delete;
    void talk() { std::cout << mData << " > i am " << type() << std::endl; } 
    std::string getData() { return mData; }
    void setData(std::string const& v) { mData = v; }
protected:
    virtual std::string type() = 0;
    std::string mData;
};

class Deriv1 : public Elem {
public:
    Deriv1() { std::cout << "Deriv1()" << std::endl; }
    ~Deriv1() { std::cout << "~Deriv1()" << std::endl; }
    Deriv1(Deriv1 const& cp) {
        std::cout << "Deriv1(Deriv1)" << std::endl;
        *this = cp;
    }
    Deriv1& operator=(Deriv1 const& cp) {
        std::cout << "Deriv1::operator=" << std::endl;
        if (this != &cp) { 
            Elem::operator=(cp);           
        }
        return *this;
    }
    // remove the chance to move
    Deriv1(Deriv1&& mv) = delete;
    Deriv1& operator=(Deriv1&& mv) = delete;
protected:
    std::string type() override { return "derived 1"; }
};

class Deriv2 final : public Deriv1 {
public:
    Deriv2() { std::cout << "Deriv2()" << std::endl; }
    ~Deriv2() { std::cout << "~Deriv2()" << std::endl; }
    Deriv2(Deriv2 const& cp) {
        std::cout << "Deriv2(Deriv2)" << std::endl;
        *this = cp;
    }
    Deriv2& operator=(Deriv2 const& cp) {
        std::cout << "Deriv2::operator=" << std::endl;
        if (this != &cp) {
            Deriv1::operator=(cp);
        }
        return *this;
    }
    // remove the chance to move
    Deriv2(Deriv2&& mv) = delete;
    Deriv2& operator=(Deriv2&& mv) = delete;
protected:
    std::string type() override { return "derived 2"; }
};



class Factory {
public:
    static std::unique_ptr<Elem> create(std::string const& type) {
        std::cout << "Factory::create a " << type << std::endl;
        if (type == "deriv1")
            return std::unique_ptr<Elem>(new Deriv1());
        else if (type == "deriv2")
            return std::unique_ptr<Elem>(new Deriv2());
        std::ostringstream os;
        os << "factory: unknown type " << type;
        throw std::runtime_error(os.str());
    }   
private:
    Factory(std::string const& type = "");
};


#endif
