#include "factory.hpp"

#include <iostream>

// compiles with a simple $ g++ -I. main.cpp
// runs with a simple $ ./a.out
int main() {

    std::cout << "--- two normal elems --- " << std::endl;
    
    Deriv1 d1;
    Deriv2 d2;
    d1.setData("ciao");
    d2.setData("hello");
    d1.talk();
    d2.talk();

    std::cout << "--- two factory elems --- " << std::endl;
    auto e1 = Factory::create("deriv1");
    auto e2 = Factory::create("deriv2");
    e1->setData("hola");
    e2->setData("guten tag");
    e1->talk();
    e2->talk();

    return 0;
}

