
#include "factory.hpp"

#include <iostream>
#include <vector>
#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;


PYBIND11_MODULE(factory, m) {

    py::class_<Elem>(m, "Elem")
        .def("talk",  & Elem::talk)
        .def_property("data", &Elem::getData, &Elem::setData)
        .def_static("create", &Factory::create);
    py::class_<Deriv1, Elem>(m, "Deriv1")
        .def(py::init<>());

    py::class_<Deriv2, Deriv1>(m, "Deriv2")
        .def(py::init<>());

}


