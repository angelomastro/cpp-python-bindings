
assumptions on the project structure

```
    pybind11 (git master)
    factory
        |__ main.cpp
        |__ CMakeLists.txt
        |__ test.py
        |__ setup.py

```


compile with 

```
    pip install factory/
```


launch with

```
    python
    >> from factory import Elem, Deriv1, Deriv2
    >> e = Elem.create("deriv1") 
    >> ...
```


or execute the test `python factory/test.py`
