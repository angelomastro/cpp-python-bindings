from factory import Elem, Deriv1, Deriv2

# create elements
print("--- two normal elems --- ")
d1 = Deriv1()
d2 = Deriv2()
d1.data = "ciao"
d2.data = "hello"
d1.talk()
d2.talk()


print("--- two elems from factory ---")
e1 = Elem.create("deriv1")
e2 = Elem.create("deriv2")
e1.data = "hola"
e2.data = "guten tag"


group = [e1, e2]

group[0].data = "eu eu eu eu"
group[1].data = "uk uk uk uk"

e1.talk()
e2.talk()


