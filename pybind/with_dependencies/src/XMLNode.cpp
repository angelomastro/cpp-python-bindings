#include <XMLNode.hpp>

#include <libxml/parserInternals.h>
#include <stdexcept>

namespace xml
{
    // nothing to do for XMLNode. pure header file

	XMLNode parseRootNode(std::string const& xmlFilePath)
	{
		auto context = xmlCreateFileParserCtxt(xmlFilePath.c_str());
		if (context == nullptr) 
			throw std::runtime_error("Could not create parser context");
		if (context->directory == nullptr)
			context->directory = xmlParserGetDirectory(xmlFilePath.c_str());
		if (xmlParseDocument(context) != 0)
			throw std::runtime_error("Failed parsing file");

		return xml::XMLNode{xmlDocGetRootElement(context->myDoc)};
	}

}
