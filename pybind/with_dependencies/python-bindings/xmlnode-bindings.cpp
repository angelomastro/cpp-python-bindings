#include <XMLNode.hpp>
#include <libxml/tree.h>
#include <string>
#include <sstream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

PYBIND11_MODULE(xmlnode, m)
{
	m.doc() = "pybind11 of XMLNode.cpp";

	py::class_<xml::XMLNode>(m, "Node")
		.def(py::init<>())
                .def("getName", &xml::XMLNode::getName)
		.def("findAll", &xml::XMLNode::findAll)
		.def("getContent", &xml::XMLNode::getContent)
                .def("__repr__", [](xml::XMLNode const& n) {
                    std::ostringstream oss;
                    oss << "root node: " << n.getName();
                    return oss.str();
                });

	m.def("parseRootNode", &xml::parseRootNode, "get the root Node of an xml file");
}
