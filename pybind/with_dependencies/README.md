# cmake library with dependencies from external library and complex class structure

I added the submodule **pybind11** in order to make it easier to find the bindings just for this example. So first of all do a `git submodule init` to download it. 

This part builds a real library with real dependencies. It's an xml parser using `libxml2`. It contains a c++ test on a sample xml with with some planets and moons. 

```
git submodule update
mkdir build
cd build
cmake .. && make
```

Will get you a bunch of files. `lib_xmlnode.a` static and `test_xmlnode` the c++ test, and `sample.xml`, the file to be parsed. The first test is just this.

```
$ ./test_xmlnode 
there are 2 planets

the moons of Mars are: 2
    Phobos
    Deimos
```

*Quick test*. `python3 -c "import xmlnode; r = xmlnode.parseRootNode(\"sample.xml\"); print(r.getName())"` should print "starsystem".


*Slow test*. In **python3** terminal you can try the following

```
python3
>>> import xmlnode
>>> root = xmlnode.parseRootNode("sample.xml")
>>> root
root node: starsystem
>>> root.getName()
'starsystem'
>>> for moon in root.findAll("planets")[0].findAll("planet")[1].findAll("moons")[0].findAll("moon"):
...     print(moon.getContent())
... 

    Phobos

    Deimos

```


*Note*. The real way to do is to replace `add_subdirectory(pybind11)` in the CMakeLists.txt with `find_package(pybind11 REQUIRED)` - which implies you to update the path to point to the `pybind11/pybind11.h` as well as to the `Python.h`. In my case it was complicated to do it. 

