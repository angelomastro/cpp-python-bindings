#include <XMLNode.hpp>

#include <iostream>
#include <stdexcept>

int main()
{

    std::string filePath("sample.xml");
	xml::XMLNode root = xml::parseRootNode(filePath);

	try 
	{
	    auto planet_set = root.findAll("planets");
		auto planets = planet_set[0].findAll("planet");
		std::cout << "there are " << planets.size() 
				  << " planets\n" << std::endl;

		xml::XMLNode mars = planets[1];
	    auto mars_moon_set = mars.findAll("moons");

		auto mars_moons = mars_moon_set[0].findAll("moon");
	    std::cout << "the moons of Mars are: " 
				  << mars_moons.size() << std::endl;
		for (auto moon : mars_moons)
			std::cout << moon.getContent() << std::endl;
	}
	catch (std::exception const& e)
	{
		std::cerr << "wtf: " << e.what() << std::endl;
	}

    return 0;
}
