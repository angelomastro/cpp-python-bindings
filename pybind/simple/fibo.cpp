#include "fibo.hpp"
#include <iostream>

namespace mate
{

Fibo::Fibo(int cur) : a(0), b(-1)
{
    if (cur > 0) 
        while (cur > next());
}

int Fibo::next()
{
    if (a == 0 && b == -1)
    {
        b = 0;
        return 0;
    }
    else if (a == 0 && b == 0)
    {
        a = 1;
        return 1;
    }
    int c = a;
    a += b;
    b = c;
    return a;
}

int Fibo::cur() const
{
    return a;
}


void printUntil(int num)
{
    Fibo f;
    int n = f.next();
    std::cout << "fibonacci series until " << num << std::endl;
    while (n <= num)
    {
        std::cout << n << std::endl;
        n = f.next();
    }
}

}
