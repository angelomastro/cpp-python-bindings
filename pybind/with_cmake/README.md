# cmake example

I added the submodule **pybind11** in order to make it easier to find the bindings just for this example. So first of all do a `git submodule init` to download it. 

*Note*. The real way to do is to replace `add_subdirectory(pybind11)` in the CMakeLists.txt with `find_package(pybind11 REQUIRED)` - which implies you to update the path to point to the `pybind11/pybind11.h` as well as to the `Python.h`. In my case it was complicated to do it. 


Run this to build.
```
mkdir build
cd build
cmake ..
make
```

*Quick test*. `python3 -c "import fibo; print(fibo.Fibo(8).next())"` should print 13. 


*Slow test*. Open `python` from the terminal, should print the following. **Python3**

```
>>> import fibo
>>> f = fibo.Fibo(5)
>>> f
<fibo.Fibo, current=5>
>>> f.next() 
8L
>>> f
<fibo.Fibo, current=8>
>>> f.printUntil(40)
```
Shoud print all numbers until 34. 

