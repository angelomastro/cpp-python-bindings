#include <fibo.hpp>

#include <pybind11/pybind11.h>
namespace py = pybind11;

#include <sstream>

PYBIND11_MODULE(fibo, m)
{
    m.doc() = "pybind11 of fibo.cpp";
    
    py::class_<mate::Fibo>(m,"Fibo")
        .def(py::init<int>(), py::arg("num") = 0)
        .def("next", &mate::Fibo::next)
        .def("cur", &mate::Fibo::cur)
        .def("__repr__", [](mate::Fibo const& f) 
            {
                std::ostringstream oss;
                oss << "<fibo.Fibo, current=" << f.cur() << ">";
                return oss.str();
            });

    m.def("printUntil", &mate::printUntil, "print fibonacci sequence until this number");
}
