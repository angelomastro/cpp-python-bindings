from dynamic_type import Container, Elem, Deriv1, Deriv2

# create elements
d1 = Deriv1()
d2 = Deriv2()

# test
d1.talk()
d2.talk()

# put them into a container
c = Container()
c.addElem(d1)
c.addElem(d2)

#print("len(c) = %d" % len(c))

# query and modify them
#c.elems()[0].data = "ciao"
#c[1].data = "bonjour"

# alternatively
#c.elems[0].data = "ciao"
#c.elems[1].data = "bonjour"

# the only trick that works is operator[] for the class
#c[0].data = "ciao"
#f = c[1]
#f.data = "bonjour"


print("the moment of truth")
print("1: %s" % c.elems)
print("2: %s" % c.elems)

c.elems[0].data = "ciao"
c.elems[1].data = "bonjour"

c.talk()


f = c.elems[0]
g = c.elems[1]

f.data = "hola"
g.data = "guten tag"

print("the moment of truth again")
print("1: %s" % c.elems)
c.talk()


