
assumptions on the project structure

```
    pybind11 (git master)
    dynamic_type
        |__ main.cpp
        |__ CMakeLists.txt
        |__ test.py
        |__ setup.py

```


compile with 

```
    pip install dynamic_type/
```


launch with

```
    python
    >> from dynamic_type import Container, Elem, Deriv1, Deriv2
    >> ... 
```


or execute the test `python dynamic_type/test.py`
