#include "dynamic_type.hpp"

#include <iostream>

// compiles with a simple $ g++ -I. main.cpp
// runs with a simple $ ./a.out
int main() {

    Deriv1 d1;
    Deriv2 d2;

    d1.talk();
    d2.talk();

    Container c;
    c.addElem(d1);
    c.addElem(d2);

    size_t sz =  c.getElems().size();
    std::cout << "size of c: " << sz << std::endl;

    auto items = c.getElems();

    items[0]->setData("ciao");
    items[1]->setData("bonjour");

    for (size_t i = 0; i < items.size(); ++i)
        items[i]->talk();

    return 0;
}

