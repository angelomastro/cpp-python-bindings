
#include "dynamic_type.hpp"

#include <iostream>
#include <vector>
#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

using VectorElemPtr = std::vector<std::shared_ptr<Elem>>;


PYBIND11_MODULE(dynamic_type, m) {

    py::class_<Elem, std::shared_ptr<Elem>>(m, "Elem")
        .def("talk",  & Elem::talk)
        .def_property("data", &Elem::getData, &Elem::setData);

    py::class_<Deriv1, std::shared_ptr<Deriv1>, Elem>(m, "Deriv1")
        .def(py::init<>());

    py::class_<Deriv2, std::shared_ptr<Deriv2>, Deriv1>(m, "Deriv2")
        .def(py::init<>());


    // this works but it's an overkill (just to show how operator[] and size() can be translated to python
    //py::class_<VectorElemPtr>(m, "VectorElemPtr")
    //    .def(py::init<>())
    //    .def("__len__", [](VectorElemPtr const& v){ return v.size();})
    //    .def("__getitem__", [](VectorElemPtr const& v, size_t idx) -> py::object {
    //            return py::object(py::cast(v[idx]));});

    py::class_<Container>(m, "Container")
        .def(py::init<>())
        .def_property("elems", 
                (std::vector<std::shared_ptr<Elem>> const& (Container::*)() const) & Container::getElems,
                (std::vector<std::shared_ptr<Elem>> & (Container::*)()) & Container::getElems)
        // the derived function should go first or the above
        .def("addElem", (void (Container::*)(Deriv2 const&)) & Container::addElem)
        .def("addElem", (void (Container::*)(Deriv1 const&)) & Container::addElem)
        .def("talk", &Container::talk);

}


