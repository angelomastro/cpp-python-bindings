#ifndef DYNAMIC_TYPE_HPP
#define DYNAMIC_TYPE_HPP

#include <iostream>
#include <vector>
#include <memory>

class Elem {
public:
    Elem() { std::cout << "Elem()" << std::endl; }
    virtual ~Elem() { std::cout << "~Elem()" << std::endl;}
    Elem(Elem const& cp) {
        std::cout << "Elem(Elem)" << std::endl;
        *this = cp;
    }
    Elem& operator=(Elem const& cp) {
        std::cout << "Elem::operator=" << std::endl;
        if (this != &cp) {
        }
        return *this;
    }
    void talk() { std::cout << mData << " > i am " << type() << std::endl; } 
    std::string getData() { return mData; }
    void setData(std::string const& v) { mData = v; }
protected:
    virtual std::string type() = 0;
    std::string mData;
};

class Deriv1 : public Elem {
public:
    Deriv1() { std::cout << "Deriv1()" << std::endl; }
    ~Deriv1() { std::cout << "~Deriv1()" << std::endl; }
    Deriv1(Deriv1 const& cp) {
        std::cout << "Deriv1(Deriv1)" << std::endl;
        *this = cp;
    }
    Deriv1& operator=(Deriv1 const& cp) {
        std::cout << "Deriv1::operator=" << std::endl;
        if (this != &cp) { 
            Elem::operator=(cp);           
        }
        return *this;
    }
protected:
    std::string type() override { return "derived 1"; }
};

class Deriv2 final : public Deriv1 {
public:
    Deriv2() { std::cout << "Deriv2()" << std::endl; }
    ~Deriv2() { std::cout << "~Deriv2()" << std::endl; }
    Deriv2(Deriv2 const& cp) {
        std::cout << "Deriv2(Deriv2)" << std::endl;
        *this = cp;
    }
    Deriv2& operator=(Deriv2 const& cp) {
        std::cout << "Deriv2::operator=" << std::endl;
        if (this != &cp) {
            Deriv1::operator=(cp);
        }
        return *this;
    }
protected:
    std::string type() override { return "derived 2"; }
};


class Container {
public:

    Container() { std::cout << "Container()" << std::endl;}
    ~Container() {std::cout << "~Container()" << std::endl;}
    Container(Container const& cp) = default;
    Container& operator=(Container const& cp) = default;

    std::vector<std::shared_ptr<Elem>> const& getElems() const { 
        std::cout << "getElems const&" << std::endl;
        return mElems;
    }
    std::vector<std::shared_ptr<Elem>>& getElems() {
        std::cout << "getElems &" << std::endl;
        return mElems;
    }

    void addElem(Deriv1 const& val) {
        std::cout << "addElem(Deriv1)" << std::endl;
        mElems.push_back(std::move(std::shared_ptr<Elem>(new Deriv1(val))));
    }
    void addElem(Deriv2 const& val) {
        std::cout << "addElem(Deriv2)" << std::endl;
        mElems.push_back(std::move(std::shared_ptr<Elem>(new Deriv2(val))));
    }
    void addElem(std::shared_ptr<Elem> val) {
        std::cout << "addElem(ElemPtr)" << std::endl;
        mElems.push_back(std::move(val));
    }

    void talk() {
        for (size_t i = 0; i < mElems.size(); ++i) 
            mElems[i]->talk();
    }

private:
    std::vector<std::shared_ptr<Elem>> mElems;
};


//// this is just to show how to override an STL container and bind it 
//template <class T>
//class VectorPtr : std::vector<std::shared_ptr<T>> {
//public:
//    VectorPtr<T>() = default;
//    VectorPtr<T>(VectorPtr<T> const&) = default;
//    VectorPtr<T>& operator=(VectorPtr<T> const& cp) = default;
//    ~VectorPtr() = default;
//
//    VectorPtr<T>(VectorPtr<T>&& mv) = delete;
//    VectorPtr<T>& operator=(VectorPtr<T>&& mv) = delete;
//
//    T* operator[](size_t idx) const {
//        if (idx >= data.size())
//            return nullptr;
//        return data[idx].get();
//    }
//
//    T* operator[](size_t idx) {
//        if (idx >= data.size())
//            return nullptr;
//        return data[idx].get();
//    }
//
//    void push_back(T* p) {
//        data.push_back(std::shared_ptr<T>(p));
//    }
//
//    void push_back(std::shared_ptr<T> cp) {
//        data.push_back(cp);
//    }
//
//    size_t size() const {
//        return data.size();
//    }
//
//private:
//    std::vector<std::shared_ptr<T>> data;
//};



#endif
