## PyBind11 to make python bindings of a C++ library

Three cases are showed, simple [gcc](/simple), [cmake](/with_cmake) and [complex-structure](/with_dependencies).

### requirements

You may need a few `apt-get update` before every command. 

```
# Ubuntu 16.04

# submodules: download them too
git submodule init
git submodule update

# gcc (5+)
sudo apt-get install g++

# python (python3 recommended for the cmake example)
sudo apt-get install python-dev python-pip -y
sudo apt-get install python3-dev python3-pip -y


# pybind11 with pip
#          will install it into /usr/local/pythonX.YY 
#          use python3 for cmake example  
sudo pip install pybind11
sudo pip3 install pybind11 

```


### play with examples

all work more or less like that 

```
cd factory
cd build
cmake ..
make -j8
./cppfactory # built and executed the cpp

pip install ../
python -c "from factory import *"
... 
```



*Note*. Make sure pip installed pybind11 in the right place (a python2.7 or python3.x directory, non-user specific).
