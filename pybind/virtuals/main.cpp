
#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

class Animal {
public:
    ~Animal() = default;
    std::string talk() { return makesound(); } 
private:
    virtual std::string makesound() = 0;
};

class Dog : public Animal {
private:
    std::string makesound() override { return "woof!"; }
};



PYBIND11_MODULE(example, m) {

    py::class_<Animal> animal(m, "Animal");
        animal.def("talk", &Animal::talk);

    py::class_<Dog, Animal>(m, "Dog")
        .def(py::init<>());

}



//int main() {
//
//    Animal* p = new Dog();
//    std::cout << p->talk() << std::endl;
//    delete p;
//
//    return 0;
//}
