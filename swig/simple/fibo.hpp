#ifndef FIBO_H
#define FIBO_H

namespace mate
{

class Fibo
{
public:
    Fibo(int cur=0);

    int next();

private:
    int a, b;
};


void printUntil(int num);

}

#endif //FIBO_H
