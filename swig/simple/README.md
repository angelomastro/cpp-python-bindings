# simple example


All libraries are in one file, I compile from there and run the example from there.

I use c++ with a few feature (classes and namespace, to begin with), in order to enrich a little bit the millions of (not so useful) C examples you find on google. 


Use `make cpp` to build the c++ library and test executable. If this works and prints numbers, you can have high hopes.


Next, build it to python with `make`. If no errors, you can carry on with python code. 


*Quick test*. `python -c "import fibo; print(fibo.Fibo(8).next())"` should print 13. 


*Slow test*. Open `python` from the terminal.

```
>>> import fibo
>>> f = fibo.Fibo(5)
>>> f.next() 
```
Should print 8.

```
>>> f.printUntil(40)
```
Shoud print all numbers until 34. 


