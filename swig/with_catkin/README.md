# catkin example


This required to have created a catkin workspace, and inside the *src* folder add one catkin package called *fibo*, and finally use the ***very easy*** to manage catkin CMakeLists.txt.

Build the repo with `catkin_make`. You now have the *build* and *devel* folders. 

Launch the cpp test with `./devel/lib/fibo/test_fibo` and it should print stuff. 


For python, you have to add the path containing the **so** file and the **py** file, which is inside *build/fibo*.


*Quick test*.  `python -c "import sys; sys.path.append(\"build/fibo\"); import fibo; print(fibo.Fibo(8).next())"` should print 13.

*Slow test*. In the build directory you should be able to find the python lib, and from there, again, test like this. 

```
>>> import sys
>>> sys.path.append("build/fibo");
>>> import fibo
>>> f = fibo.Fibo(5)
>>> f.next() 
```
Should print 8.

```
>>> f.printUntil(40)
```
Shoud print all numbers until 34. 


