# cmake example


**Forreal**. The project is structured into *src* and *include* directories, and I makde the example of one-of-many projects (*src/project_1* and so on ...). 


And I use C++11, because [*it's 2015!*](https://www.youtube.com/watch?v=LLk2aSBrR6U), but if you comment out `set(CMAKE_CXX_STANDARD 11)` and use another compiler, things will work just the same; *here too* this tool is just meant to show off. 


Build with `mkdir build && cd build && cmake .. && make`. Run the `test_fibo` and if the numbers print out, it's a good start. 

*Quick test*.  `python -c "import fibo; print(fibo.Fibo(8).next())"` should print 13.

*Slow test*. In the build directory you should be able to find the python lib, and from there, again, test like this. 

```
>>> import fibo
>>> f = fibo.Fibo(5)
>>> f.next() 
```
Should print 8.

```
>>> f.printUntil(40)
```
Shoud print all numbers until 34. 


