## How to make python bindings of a C++ library

Three cases are showed, simple ([gcc](/simple)), [cmake](/with_cmake) and [catkin](/with_catkin).

### requirements

You may need a few `apt-get update` before every command. 

```
# Ubuntu 16.04

# SWIG
sudo apt-get install swig

# gcc (5+)
sudo apt-get install g++

# python 2
sudo apt-get install python-dev 

# catkin with ROS
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
sudo apt-get update
sudo apt-get install ros-kinetic-desktop-full

```

Note: for ROS you can read more [here](http://wiki.ros.org/kinetic/Installation/Ubuntu).
