project(fibo)

cmake_minimum_required(VERSION 3.5)

set(CMAKE_CXX_STANDARD 11)

# Building the normal executable test_fibo:
#     build a static (or shared) lib "fibo.a", then link it to the "test_fibo" exec
include_directories(include/${PROJECT_NAME}) # will not show in QT
add_library(${PROJECT_NAME} STATIC src/${PROJECT_NAME}/${PROJECT_NAME}.cpp) 
add_executable(test_${PROJECT_NAME}
               #include/${PROJECT_NAME}  # will show headers in QT
               src/${PROJECT_NAME}/test_${PROJECT_NAME}.cpp)
target_link_libraries(test_${PROJECT_NAME} ${PROJECT_NAME})


# Building the python-bindings
## generic 
find_package(SWIG REQUIRED)
find_package(PythonInterp REQUIRED)
include(${SWIG_USE_FILE})
find_package(PythonLibs REQUIRED)
include_directories(${PYTHON_INCLUDE_PATH} ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_SWIG_FLAGS "")

## specific to the library being converted
set(BINDING_CONFIG python/${PROJECT_NAME}/${PROJECT_NAME}.i) # .i file to write manually
##include_directories(include/${PROJECT_NAME})
set_source_files_properties(${BINDING_CONFIG} PROPERTIES CPLUSPLUS ON)
set_source_files_properties(${BINDING_CONFIG} PROPERTIES SWIG_FLAGS "-includeall")
file(GLOB ${PROJECT_NAME}_HEADERS include/${PROJECT_NAME}/*.h*)
SWIG_ADD_MODULE(${PROJECT_NAME}
                python 
                ${BINDING_CONFIG}
                ${${PROJECT_NAME}_HEADERS}
                src/${PROJECT_NAME}/${PROJECT_NAME}.cpp)
SWIG_LINK_LIBRARIES(${PROJECT_NAME}
                    ${PYTHON_LIBRARIES})


# installing this beaty
execute_process(COMMAND python -c "from distutils import sysconfig; print(sysconfig.get_python_lib(1,0,prefix=''))"
                OUTPUT_VARIABLE PYTHON_INSTDIR
                OUTPUT_STRIP_TRAILING_WHITESPACE)
## install locally (in specific directories)
SET_TARGET_PROPERTIES(_${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
SET_TARGET_PROPERTIES(_${PROJECT_NAME} PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
## install globally as a python module
#INSTALL(TARGETS _${PROJECT_NAME} DESTINATION lib/python2.7/dist-packages)
#INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.py DESTINATION lib/python2.7/dist-packages)
### note: if you want to make a package instead of a module (done above):
###     1. install "dist-packages/fibo"
###     2. put __init__.py inside of it
###     3. install _fibo.so and fibo.py inside of it as before
###     Now you can open "python" and ">>> from fibo import fibo" 

