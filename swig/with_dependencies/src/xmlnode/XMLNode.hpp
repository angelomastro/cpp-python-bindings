#ifndef XMLNODE_HPP
#define XMLNODE_HPP

#include <libxml/tree.h>
#include <vector>

namespace xml {

    class XMLNode {
    public:
        class Iterator;

        using iterator = Iterator;

        explicit XMLNode(xmlNode *node) : mNode{node} {}

        explicit operator bool() const { return mNode != nullptr; }

        /// Get next sibling
        XMLNode getNext() const { return XMLNode{mNode->next}; }

        /// Get tag name
        std::string getName() const { return reinterpret_cast<char const*>(mNode->name); }

        /// Get child nodes
        XMLNode getChildren() const { return XMLNode{mNode->children}; }

        /// Check whether a property is present
        bool hasProp(std::string const &name) const;

        /// Get property value
        std::string getProp(std::string const &name) const;

        /// Get element text content
        std::string getContent() const;

        /// Finds the first child node with @name
        XMLNode find(std::string const &name) const;

        /// Finds all child nodes with @name
        std::vector<XMLNode> findAll(std::string const &name) const;

        /// Finds first descendant nodes with @name
        XMLNode findDeep(std::string const &name) const;

        /// Finds all descendant nodes with @name
        std::vector<XMLNode> findAllDeep(std::string const &name) const;

        /// Beginning for iterating over children
        iterator begin() { return Iterator{mNode->children}; }

        /// End for iterating over children
        iterator end() { return Iterator{nullptr}; }

    private:
        xmlNode *mNode;

        void findAllDeep(std::vector<XMLNode> &res, std::string const &name) const;

    public:
        /// Helper class to iterate over nodes
        class Iterator {
        public:
            Iterator &operator++();
            Iterator &operator--();
            XMLNode operator*() const;
            bool operator==(Iterator const &other) const;
            bool operator!=(Iterator const &other) const;

        private:
            friend class XMLNode;

            explicit Iterator(xmlNode *node) : mNode{node} {}

            xmlNode *mNode;
        };

    };


    inline bool XMLNode::hasProp(std::string const &name) const {
        return xmlHasProp(mNode, reinterpret_cast<xmlChar const *>(name.c_str())) != nullptr;
    }

    inline std::string XMLNode::getProp(std::string const &name) const {
        xmlChar *xprop = xmlGetProp(mNode, reinterpret_cast<xmlChar const *>(name.c_str()));
        if (xprop == nullptr)
            return "";
        std::string prop(reinterpret_cast<char *>(xprop));
        xmlFree(xprop);
        return prop;
    }

    inline std::string XMLNode::getContent() const {
        xmlChar *c = xmlNodeGetContent(mNode);
        std::string content(reinterpret_cast<char *>(c));
        xmlFree(c);
        return content;
    }

    inline XMLNode XMLNode::find(std::string const &name) const {
        for (XMLNode i = getChildren(); i; i = i.getNext())
            if (i.getName() == name)
                return i;
        return XMLNode{nullptr};
    }

    inline std::vector<XMLNode> XMLNode::findAll(std::string const &name) const {
        auto res = std::vector<XMLNode>{};
        for (XMLNode i = getChildren(); i; i = i.getNext())
            if (i.getName() == name)
                res.push_back(i);
        return res;
    }

    inline std::vector<XMLNode> XMLNode::findAllDeep(std::string const &name) const {
        auto res = std::vector<XMLNode>{};
        findAllDeep(res, name);
        return res;
    }

    inline void XMLNode::findAllDeep(std::vector<XMLNode> &res, std::string const &name) const {
        for (XMLNode i = getChildren(); i; i = i.getNext()) {
            i.findAllDeep(res, name);

            if (i.getName() == name)
                res.push_back(i);

        }
    }

    inline XMLNode XMLNode::findDeep(std::string const &name) const {
        for (XMLNode i = getChildren(); i; i = i.getNext()) {
            if (i.getName() == name)
                return i;
            else {
                XMLNode p = i.findDeep(name);
                if (p)
                    return p;
            }
        }

        return XMLNode{nullptr};
    }


    inline XMLNode::Iterator &XMLNode::Iterator::operator++() {
        mNode = mNode->next;
        return *this;
    }

    inline XMLNode::Iterator &XMLNode::Iterator::operator--() {
        mNode = mNode->prev;
        return *this;
    }

    inline XMLNode XMLNode::Iterator::operator*() const {
        return XMLNode{mNode};
    }

    inline bool XMLNode::Iterator::operator==(Iterator const &other) const {
        return mNode == other.mNode;
    }

    inline bool XMLNode::Iterator::operator!=(XMLNode::Iterator const &other) const {
        return mNode != other.mNode;
    }

}

#endif //XMLNODE_HPP
