/* interface file for SWIG */

%module xmlnode
%include <std_vector.i>

/* %ignore xml::XMLNode; */ /*swig friend classes */
/*namespace xml {*/
/*
class XMLNode; /*swig nested classes*/
class Iterator { /*swig nested classes*/
public:
    Iterator &operator++();
    Iterator &operator--();
    XMLNode operator*() const;
    bool operator==(Iterator const &other) const;
    bool operator!=(Iterator const &other) const;
private:
    friend class XMLNode;
    explicit Iterator(xmlNode *node) : mNode{node} {}
    xmlNode *mNode;
};
%rename(__next__) Iterator::operator++;
%rename(__prev__) Iterator::operator--;
%rename(value) Iterator::operator*;
*/

/*}*/

%{
#include "../src/xmlnode/XMLNode.hpp"
%}
%include "../src/xmlnode/XMLNode.hpp"

/*
%{  /*swig nested classes*/
    typedef xml::XMLNode::Iterator Iterator;
%}
*/
