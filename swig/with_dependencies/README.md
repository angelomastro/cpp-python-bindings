# dependencies example


There is an xml node parsers in the source, and we want to make it into a python xml parser. It depends on  **libxml2** and **std::vector**. So, the CMakeLists.txt and xmlnode.i files must be updated acccordingly, and the environment must contain headers from `sudo apt-get install libxml2-dev`. 


*Build*. It's a cmake project, so the usual `mkdir build && cd build && cmake .. && make`. 


*Quick test*.  `python -c "import xx; xx.dosomething()"` should do something.

